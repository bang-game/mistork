import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  final String helloText;

  MyHomePage({Key key, this.helloText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(helloText),
      ),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 200,
              width: 200,
              child: Center(child: Text("dfgdfg")),
              color: Colors.green,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Container(
                height: 100,
                width: 100,
                child: Center(child: Text("dfgdfg")),
                color: Colors.red,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          showDialog(
            context: context,
            child: AlertDialog(
              title: Text("onPress"),
              content: Text("content"),
            ),
            barrierDismissible: false,
          );
        },
      ),
    );
  }
}
